var mymap = L.map('mapa').setView([-34.6184879,-58.3907281], 12);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1
}).addTo(mymap);

// Creo el icono

var myIcon = L.icon({
    iconUrl: 'images/marcador_mapa_bici.png',
    iconSize: [38, 53],
    iconAnchor: [22, 50],
    popupAnchor: [-3, -76],
});


$.ajax({
    method: 'POST',
    dataType: 'json',
    url: 'api/auth/authenticate',
    data: { email: 'hector10@gmail.com', password: 'Yerbal3032' },
}).done(function( data ) {
    console.log(data);

    $.ajax({
        dataType: 'json',
        url: 'api/bicicletas',
        beforeSend: function (xhr) {
            xhr.setRequestHeader("x-access-token", data.data.token);
        }
    }).done(function (result) {
        console.log(result);

        result.bicicletas.forEach(bici => {
            L.marker(bici.ubicacion, {icon: myIcon}, {title: `Bici #${bici.code}`}, {alt:'Marcador Red 1'}).addTo(mymap)
                .bindPopup(`Bici con Id ${bici.code}`)
                .openPopup();
        });
    });
});
//cargo puntos harcodeados para probar
