const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookTokenStrategy = require("passport-facebook").Strategy;
const Usuario = require('../models/usuario');

passport.use(
    new FacebookTokenStrategy(
      {
        clientID: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET,
        callbackURL: process.env.HOST + "/auth/facebook/callback",
        profileFields: ["id", "displayName", "name", "emails"],
      },
      function (accessToken, refreshToken, profile, cb) {
        User.findOneOrCreateByFacebook(profile, function (err, user) {
          return cb(err, user);
        });
      }
    )
);
passport.use(new LocalStrategy(
    function(email, password, done){
        Usuario.findOne({email: email}, function(err, usuario) {
            if(err) return done(err);
            if(!usuario) return done(null, false, {message:'email no existe o incorrecto'});
            if(!usuario.validPassword(password)) return done(null,false,{message:'password incorrecto'});

            return done(null, usuario);
        });
    }
));

passport.use(new GoogleStrategy({
    clientID:     process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback"
  },
	  function(accessToken, refreshToken, profile, cb) {
	  	console.log(profile);

	    Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
	      return cb(err, user);
	    });
	})
);

passport.serializeUser(function( user, cb ){
    cb(null, user.id);
});

passport.deserializeUser(function(id,cb){
    Usuario.findById(id, function( err, usuario ){
        cb(null, usuario);
    })
});

module.exports = passport