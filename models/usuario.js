var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Token = require('./token');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');
var crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
const saltRound = 10;

const mailer = require('../mailer/mailer');

const validateEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
}

var usuarioSchema = new Schema({
    nombre : {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email : {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        validate: [validateEmail, 'Por favor ingresar un email válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpire: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function(next){
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRound);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

usuarioSchema.method.allUsers = function( req, res ){
    return this.find({},cb);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({ usuario:this._id, bicicleta:biciId, desde:desde, hasta:hasta });
    //console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.statics.add = function(aUser, cb){
    console.log(aUser);
    this.create(aUser,cb);
};

usuarioSchema.statics.updateUser = function(userObj, cb){
    //console.log(userObj);
    console.log(userObj.nombre);
    this.updateOne({ _id: userObj._id }, {$set: {nombre: userObj.nombre}}, cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({_userId: this._id,token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;

    console.log('token => ' + token);
    console.log('email destination => ' + email_destination);

    token.save(function( err ){
        if( err ) {
            return console.log(err.message);
        }
        
        const mailOptions = {
            from: 'no-reply@redbicicleta.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola, \n\n'+' para verificar su cuenta haga click en este enlace: \n' + 'http://localhost:3000'+'\/token/confirmation\/'+token.token + '\n' 
        }

        mailer.sendMail(mailOptions, ( err )=> {
            if(err) { return console.log(err.message); }
            console.log('a verification email has been sent to ' + email_destination);
        });
    });
};

usuarioSchema.methods.resetPassword = function(cb) {
    const token = new Token({_userId: this._id,token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;

    console.log('token => ' + token);
    console.log('token => ' + email_destination);

    token.save(function( err ){
        if( err ) {
            return console.log(err.message);
        }
        
        const mailOptions = {
            from: 'no-reply@redbicicleta.com',
            to: email_destination,
            subject: 'Reestablecer password',
            text: 'Hola, \n\n'+' para reestablecer su password haga click en este enlace: \n' + 'http://localhost:3000'+'\/resetPassword\/'+token.token + '\n' 
        }

        mailer.sendMail(mailOptions, ( err )=> {
            if(err) { return console.log(err.message); }
            console.log('a reset password email has been sent to ' + email_destination);
        });
    });
    
};
module.exports = mongoose.model('Usuario', usuarioSchema);