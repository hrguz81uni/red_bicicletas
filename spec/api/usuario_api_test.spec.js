var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');
var request = require('request');
var server = require('../../bin/www');

describe('Testing Api Usuarios ', function() {
    
    beforeEach(function(done){
        
        var mongoDB = "mongodb://192.168.0.11:.27017/testapidb";
        mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true, useCreateIndex: true, });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    describe(' POST USUARIOS /create', () => {
        it('Status 200', (done) => {
            
            var headers = {'content-type' : 'application/json'};
            
            var aUser = '{"nombre":"leo polo"}';

            request.post({
                headers : headers,
                url : 'http://localhost:3000/api/usuarios/create',
                body : aUser
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Usuario.find({ "nombre":"leo polo" }, ( err, usuario) => {
                    if ( err) console.log(err);
                    //console.log(usuario);
                    expect(usuario[0].nombre).toEqual('leo polo');
                    done();
                });
                
            });
        
        });
    });

    describe(' Reservas /reservar', () => {
        it('devuelve la información de la reserva', (done) => {
            
            let usuario = new Usuario({"nombre": "Richard Alvarez"});
            usuario.save();

            let bicicleta = new Bicicleta({"code": 1, "color":"azul","modelo":"urbano"});
            bicicleta.save();

            var headers = {'content-type' : 'application/json'};
            
            let aReserva = {
                desde: "2020-08-03",
                hasta: "2020-08-05",
                id: usuario._id,
                bici_id: bicicleta._id
            };

            request.post({
                headers : headers,
                url : 'http://localhost:3000/api/usuarios/reservar',
                body : JSON.stringify(aReserva)
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    if ( err) console.log(err);
                    //console.log(reservas);
                    //expect(usuario[0].nombre).toEqual('leo polo');
                    done();
                });
                
            });
        
        });
    });

    describe(' UPDATE USUARIOS /update', () => {
        it('actualizar una usuario', (done) => {
            
            var aUser = new Usuario({nombre:"Juan Manuel Santos"});
            
            aUser.save( function(err, user) {
                if( err ) console.log( err );
                Usuario.findById(user._id, function(err, usuario){
                    if( err ) console.log(err);
                    
                    var aUser = {
                        _id: usuario._id,
                        nombre: "Juan gabriel"
                    };

                    var headers = {'content-type' : 'application/json'};
                    var auserUpdate = JSON.stringify(aUser);

                    request.post({
                        headers : headers,
                        url : 'http://localhost:3000/api/usuarios/update',
                        body : auserUpdate
                        
                    },
                    function(error, response, body) {
                        console.log(response.statusMessage);
                        console.log(error);
                        expect(response.statusCode).toBe(200);
                        Usuario.findById(usuario._id, ( err, usuario) => {
                            if ( err) console.log(err);
                            // console.log(usuario);
                            expect(usuario.nombre).toBe("Juan gabriel");
                            done();
                        });
                    });
                });
            });        
        });
    });
});