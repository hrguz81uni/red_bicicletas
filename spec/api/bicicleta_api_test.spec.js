var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');


describe('Testing Api Bicicletas ', function() {
    
    beforeEach(function(done){
        
        var mongoDB = "mongodb://192.168.0.11:27017/testapidb";
        mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true  });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function( err, success ){
            if (err) console.log(err);
            done();
        });
    });

    describe(' GET BICILECTAS /', () => {
        it('Status 200', (done) => {
            //valida si la entidad esta vacia
            Bicicleta.allBicis(( err, bicis ) => {
                expect(bicis.length).toBe(0);
            });

            // Añadir una bicicleta
            var aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, ( err, newBici) => {
                if (err) console.log(err);
            });
            
            request.get('http://localhost:3000/api/bicicletas', (error, response, body) => {
                //console.log(Bicicleta.allBici);
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });

    describe(' POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            
            var headers = {'content-type' : 'application/json'};
            
            var aBici = '{"code":1, "color":"azul", "modelo":"Urbano", "lat":"10.974273", "lng":"-74.815885"}';

            request.post({
                headers : headers,
                url : 'http://localhost:3000/api/bicicletas/create',
                body : aBici
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                Bicicleta.findByCode(1, ( err, bicicleta) => {
                    if ( err) console.log(err);
                    //console.log(bicicleta);
                    expect(bicicleta.code).toBe(1);
                    done();
                });
                
            });
        
        });
    });

    describe(' UPDATE BICILECTAS /update', () => {
        it('actualizar una bicicleta', (done) => {
            
            var aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            
            aBici.save(( err, bici ) => {
                if (err) console.log(err);
                
                Bicicleta.findOne({_id:bici._id}, 'code color  modelo').exec(( err, bicicleta) => {
                    if ( err ) console.log( err );
                    
                    console.log(bicicleta);
                    
                    var headers = {'content-type' : 'application/json'};
                    var abiciUpdate = '{ "code":1,"color":"Red","modelo":"Urbano","lat": "10.975832","lng": "-74.808815" }';

                    console.log(bicicleta);

                    request.post({
                        headers : headers,
                        url : 'http://localhost:3000/api/bicicletas/update',
                        body : abiciUpdate
                        
                    },
                    function(error, response, body) {
                        expect(response.statusCode).toBe(200);
                        Bicicleta.findByCode(1, ( err, bicicleta) => {
                            if ( err) console.log(err);
                            // console.log('after update => ' + bicicleta);
                            expect(bicicleta.code).toBe(1);
                            done();
                        });
                    });

                });
            });
   
            // console.log('before update => ' + aBici);
        
        });
    });
        
        
});

// describe('Bicicleta API', () => {
    

//     
//     
//     describe(' DELETE BICILECTAS /update', () => {
//         it('Status 204', (done) => {
            
//             //add bici
//             var abici = new Bicicleta(11,'yellow','urbana',[10.974273, -74.815885]);
//             Bicicleta.add(abici);

//             //setting header and boy of the request
//             var headers = {'content-type' : 'application/json'};
//             var idBici = abici['id'];

//             // console.log(Bicicleta.allBici);

//             request.delete({
//                 headers : headers,
//                 url : 'http://localhost:3000/api/bicicletas/'+idBici+'/delete',    
//             },
//             function(error, response, body) {
//                 expect(response.statusCode).toBe(204);
//                 done();
//             });
        
//         });
//     });

// });